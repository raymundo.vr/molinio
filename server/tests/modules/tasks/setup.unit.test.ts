import * as SetupTasks from '../../../src/modules/tasks/setup';
import * as HttpUtil from '../../../src/lib/gitlab/http';
import { mocked } from 'ts-jest/utils';
import GitlabProject from '../../../src/modules/project/gitlab';
import { SetupResult } from '../../../src/common/types';

jest.mock('../../../src/lib/gitlab/http');

const PROJECT = new GitlabProject('https://gitlab.test.com/api', 'this-is-not-a-token', 'test-project');

describe("Setup tasks", () => {
    describe("::hasProjectBeenSetup()", () => {
        afterEach(() => {
            jest.resetAllMocks();
            jest.clearAllMocks();
        });

        test("Returns TREE_MISSING when project is empty", async () => {
            mocked(HttpUtil.getResource).mockResolvedValue({
                ok: false,
                details: { code: 404, data: { message: 'Tree Not Found' } }
            });
            const result = await SetupTasks.hasProjectBeenSetup(PROJECT);
            expect(result).toEqual(SetupResult.TREE_MISSING);
        });
    });

    describe("::verifyProjectSetup()", () => {
        afterEach(() => {
            jest.resetAllMocks();
            jest.clearAllMocks();
        });

        test("Runs initial commit and branch setup process when project is empty", async () => {
            mocked(HttpUtil.getResource).mockResolvedValue({
                ok: false,
                details: { code: 404, data: { message: 'Tree Not Found' } }
            });
            mocked(HttpUtil.postResource)
                .mockResolvedValueOnce({
                    ok: true,
                    details: { code: 201, data: { message: 'Commit created' } }
                })
                .mockResolvedValueOnce({
                    ok: true,
                    details: { code: 200, data: { message: 'Branch next created' } }
                })
                .mockResolvedValueOnce({
                    ok: true,
                    details: { code: 200, data: { message: 'Branch next protected' } }
                });

            const postSpy = jest.spyOn(HttpUtil, 'postResource');
            const protectBranchSpy = jest.spyOn(PROJECT, 'createAndProtectBranch');

            await SetupTasks.verifyProjectSetup(PROJECT);

            expect(postSpy).toHaveBeenCalledTimes(3);
            expect(postSpy).toHaveBeenCalledWith('https://gitlab.test.com/api/projects/test-project/repository/commits', 'this-is-not-a-token', expect.anything());
            expect(postSpy).toHaveBeenCalledWith('https://gitlab.test.com/api/projects/test-project/repository/branches?branch=next&ref=main', 'this-is-not-a-token');
            expect(postSpy).toHaveBeenCalledWith('https://gitlab.test.com/api/projects/test-project/protected_branches?name=next', 'this-is-not-a-token');
            expect(protectBranchSpy).toHaveBeenCalledTimes(1);
            expect(protectBranchSpy).toHaveBeenCalledWith('next', 'main');
        });
    });
});