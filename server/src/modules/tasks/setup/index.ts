import { HttpResult, SetupResult } from '../../../common/types';
import GitlabProject from '../../project/gitlab';
import { MAIN_BRANCH, NEXT_BRANCH } from '../../../common/types';
const Logger = console;

const createNextBranch = async (project: GitlabProject): Promise<HttpResult> => {
    const result = await project.createAndProtectBranch(NEXT_BRANCH, MAIN_BRANCH);
    if (!result.ok) {
        throw new Error(`Something went wrong while creating the next branch. Code: ${result.details.code}. Message: ${JSON.stringify(result.details.data)}`);
    }

    return result;
};

export const verifyProjectSetup = async (project: GitlabProject): Promise<SetupResult> => {
    Logger.info("Running project setup verification");
    const setupCheck = await hasProjectBeenSetup(project);

    if (setupCheck === SetupResult.TREE_MISSING) {
        Logger.info("Repository tree missing, initializing.");
        let result = await project.createInitialCommit();
        if (!result.ok) {
            throw new Error(`Something went wrong while creating the initial commit. Code: ${result.details.code}. Message: ${JSON.stringify(result.details.data)}`);
        }
        await createNextBranch(project);
        return SetupResult.OK;
    } else if (setupCheck === SetupResult.NEXT_MISSING) {
        Logger.info("Next branch missing, creating.");
        await createNextBranch(project);
    }
    Logger.info("Project setup verification completed.");
    return SetupResult.OK;
};

export const hasProjectBeenSetup = async (project: GitlabProject): Promise<SetupResult> => {
    const rootContents = await project.getProjectContents();

    if (rootContents.ok) {
        const { details } = await project.getBranches();
        if (details?.data && Array.isArray(details.data)) {
            const molinioBranches = details.data
                .filter(branch => [MAIN_BRANCH, NEXT_BRANCH].includes(branch.name))
                .map(({ name, protected: isProtected }) => ({ name, isProtected }))
                ;
            if (molinioBranches.length === 0) {
                return SetupResult.NO_BRANCHES;
            } else if (!molinioBranches.find(branch => branch.name === MAIN_BRANCH)) {
                return SetupResult.MAIN_MISSING;
            } else if (!molinioBranches.find(branch => branch.name === NEXT_BRANCH)) {
                return SetupResult.NEXT_MISSING
            }
        }
    } else {
        if (rootContents.details.code === 404) {
            //Project Exists but Tree does not exist
            const treeNotFound = /Tree Not Found/.test(rootContents.details.data.message);
            if (treeNotFound) {
                return SetupResult.TREE_MISSING;
            }

            throw new Error(`Project ${project.projectId} does not exist`);
        }
    }

    return SetupResult.OK;
};