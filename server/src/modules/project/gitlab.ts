import { getResource, postResource } from '../../lib/gitlab/http';

export default class GitlabProject {
    apiUrl: string;
    token: string;
    projectId: string;

    constructor(apiUrl: string, token: string, projectId: string) {
        this.apiUrl = apiUrl;
        this.token = token;
        this.projectId = projectId;
    }

    async getProjectContents(path: string = '/') {
        const listRepositoryTree = `${this.apiUrl}/projects/${this.projectId}/repository/tree?path=${path}&recursive=false`;
        return await getResource(listRepositoryTree, this.token);
    };

    async getBranches() {
        const listBranches = `${this.apiUrl}/projects/${this.projectId}/repository/branches`;
        return await getResource(listBranches, this.token);
    }

    async createInitialCommit(mainBranch = 'main') {
        const createInitialCommit = `${this.apiUrl}/projects/${this.projectId}/repository/commits`;
        const defaultSampleFileContents = JSON.stringify({
            "common.hello": {
                description: "Hello world!",
                default: "Hello world!",
                translations: {
                    es: "¡Hola mundo!"
                }
            }
        });
        const initialCommit = {
            branch: mainBranch,
            commit_message: "Molinio: initial setup",
            actions: [
                {
                    action: "create",
                    file_path: "packs/common.json",
                    content: defaultSampleFileContents
                }
            ]
        };
        return await postResource(createInitialCommit, this.token, initialCommit);
    };

    async createAndProtectBranch(branch: string, refBranch: string) {
        const createBranch = `${this.apiUrl}/projects/${this.projectId}/repository/branches?branch=${branch}&ref=${refBranch}`;
        // TODO: Verify the correct protection levels, see: https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
        const protectBranch = `${this.apiUrl}/projects/${this.projectId}/protected_branches?name=${branch}`;

        let result = await postResource(createBranch, this.token);
        if (!result.ok) {
            return result;
        }

        return await postResource(protectBranch, this.token);
    }
}