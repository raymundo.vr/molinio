import { readFile } from 'fs';
import { promisify } from 'util';
import { Configuration } from '../../common/types';

const readFileAsync = promisify(readFile);

const load = async (filePath: string): Promise<Configuration> => {
    const fileBuffer = await readFileAsync(filePath);
    const configuration: Configuration = JSON.parse(fileBuffer.toString());
    return configuration;
};

export default load;