import fetch from 'node-fetch';
import { HttpResult, QueryMethod } from '../../common/types';

export const callGitlabApi = async (method: QueryMethod, url: string, token: string, body?: object): Promise<HttpResult> => {
    const authHeader = { 
        'PRIVATE-TOKEN': token,
        'Content-Type': 'application/json'
    };

    const response = await fetch(url, {
        method,
        headers: authHeader,
        body: body ? JSON.stringify(body) : undefined
    });

    if (!response.ok) {
        return {
            ok: false,
            details: {
                code: response.status,
                data: await response.json(),
                statusText: response.statusText
            }
        };
    }

    return {
        ok: true,
        details: {
            code: response.status,
            data: await response.json()
        }
    }
};

export const getResource = async (url: string, token: string): Promise<HttpResult> => callGitlabApi('GET', url, token);

export const postResource = async (url: string, token: string, body?: object): Promise<HttpResult> => callGitlabApi('POST', url, token, body);