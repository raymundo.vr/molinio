/**
 * Interfaces, types, consts and enums
 */

export interface ProjectConfiguration {
    apiUrl: string;
    id: string;
    token: string;
}

export interface Configuration {
    project: ProjectConfiguration;
}

export interface Result {
    ok: boolean;
    details: any;
}

export interface HttpResponseData {
    message: string;
}

export interface HttpDetails {
    code: number;
    data: HttpResponseData;
    statusText?: string;
}

export interface HttpResult extends Omit<Result, 'details'> {
    details: HttpDetails;
}

export enum SetupResult {
    OK,
    NO_BRANCHES,
    MAIN_MISSING,
    NEXT_MISSING,
    TREE_MISSING
};

export type QueryMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

export const MAIN_BRANCH = 'main';

export const NEXT_BRANCH = 'next';