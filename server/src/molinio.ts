import LoadConfiguration from "./lib/configuration";
import GitlabProject from "./modules/project/gitlab";
import { verifyProjectSetup } from "./modules/tasks/setup";

const logger = console;

(async () => {
    const { CONFIG_FILE } = process.env;

    if (!CONFIG_FILE) {
        logger.error("No CONFIG_FILE specified. Cannot start.");
        process.exit(1);
    }

    const { project } = await LoadConfiguration(CONFIG_FILE);
    const gitlabProject = new GitlabProject(project.apiUrl, project.token, project.id);
    await verifyProjectSetup(gitlabProject);
})();